
FROM node:14-alpine as builder

WORKDIR /app

COPY . /app

RUN npm install --only=prod

CMD ["npm", "start"]